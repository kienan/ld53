extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

signal changed
signal reset
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func to_params():
	var params = {
		'seed': find_node("Seed").get_text(),
		'node_count': find_node("LocationCount").get_value(),
		'cargo_availability': find_node("CargoAvailability").get_value(),
		'pickup_count_range': Vector2(1, find_node("PickupCount").get_value()),
		'empty_node_chance': find_node("EmptyNodeChance").get_value(),
		'dropoff_value_range': Vector2(1, find_node("DropoffValue").get_value()),
		'dropoff_capacity_range': Vector2(1, find_node("DropoffCapacity").get_value()),
		'link_cost_range': Vector2(1, find_node("LinkWeight").get_value()),
	}
	if find_node("Infinite").is_pressed():
		params.cargo_availability = INF
	return params
	
func from_params(params):
	self.set_block_signals(true)
	find_node("Seed").set_text(params.seed)
	find_node("LocationCount").set_value(params.node_count)
	get_node("VBoxContainer/HBoxContainer2/Value").set_text("%d" % params.node_count)
	if is_inf(params.cargo_availability):
		get_node("VBoxContainer/HBoxContainer3/CargoAvailability").set_editable(false)
		get_node("VBoxContainer/HBoxContainer3/Value").set_text("INF")
		get_node("VBoxContainer/HBoxContainer3/Infinite").set_pressed(true)
	else:
		get_node("VBoxContainer/HBoxContainer3/CargoAvailability").set_editable(true)
		get_node("VBoxContainer/HBoxContainer3/CargoAvailability").set_value(params.cargo_availability)
		get_node("VBoxContainer/HBoxContainer3/Value").set_text("%2.1f" % params.cargo_availability)
		get_node("VBoxContainer/HBoxContainer3/Infinite").set_pressed(false)
	get_node("VBoxContainer/HBoxContainer4/PickupCount").set_value(params.pickup_count_range.y)
	get_node("VBoxContainer/HBoxContainer4/Value").set_text("%d" % params.pickup_count_range.y)
	get_node("VBoxContainer/HBoxContainer5/EmptyNodeChance").set_value(params.empty_node_chance)
	get_node("VBoxContainer/HBoxContainer5/Value").set_text("%d" % params.empty_node_chance)
	get_node("VBoxContainer/HBoxContainer6/DropoffValue").set_value(params.dropoff_value_range.y)
	get_node("VBoxContainer/HBoxContainer6/Value").set_text("%d" % params.dropoff_value_range.y)
	get_node("VBoxContainer/HBoxContainer7/DropoffCapacity").set_value(params.dropoff_capacity_range.y)
	get_node("VBoxContainer/HBoxContainer7/Value").set_text("%d" % params.dropoff_capacity_range.y)
	get_node("VBoxContainer/HBoxContainer8/LinkWeight").set_value(params.link_cost_range.y)
	get_node("VBoxContainer/HBoxContainer8/Value").set_text("%d" % params.link_cost_range.y)
	self.set_block_signals(false)
	emit_signal("reset", self.to_params())

func _on_RandomizeSeed_pressed():
	# 33 - 126
	var character_count = randi() % 29 + 4
	var new_seed = ""
	while character_count > 0:
		new_seed += char(randi() % 94 + 33)
		character_count -= 1
	find_node("Seed").set_text(new_seed)
	emit_signal("changed", self.to_params())


func _on_LocationCount_value_changed(value):
	get_node("VBoxContainer/HBoxContainer2/Value").set_text("%d" % value)
	emit_signal("changed", self.to_params())


func _on_CargoAvailability_value_changed(value):
	get_node("VBoxContainer/HBoxContainer3/Value").set_text("%2.1f" % value)
	emit_signal("changed", self.to_params())


func _on_CheckBox_toggled(button_pressed):
	if button_pressed:
		get_node("VBoxContainer/HBoxContainer3/Value").set_text("INF")
		get_node("VBoxContainer/HBoxContainer3/CargoAvailability").set_editable(false)
	else:
		get_node("VBoxContainer/HBoxContainer3/Value").set_text("%2.1f" %
		get_node("VBoxContainer/HBoxContainer3/CargoAvailability").get_value()
		)
		get_node("VBoxContainer/HBoxContainer3/CargoAvailability").set_editable(true)
	emit_signal("changed", self.to_params())


func _on_PickupCount_value_changed(value):
	get_node("VBoxContainer/HBoxContainer4/Value").set_text("%d" % value)
	emit_signal("changed", self.to_params())


func _on_EmptyNodeChance_value_changed(value):
	get_node("VBoxContainer/HBoxContainer5/Value").set_text("%d" % value)
	emit_signal("changed", self.to_params())


func _on_DropoffValue_value_changed(value):
	get_node("VBoxContainer/HBoxContainer6/Value").set_text("%d" % value)
	emit_signal("changed", self.to_params())


func _on_DropoffCapacity_value_changed(value):
	get_node("VBoxContainer/HBoxContainer7/Value").set_text("%d" % value)
	emit_signal("changed", self.to_params())


func _on_LinkWeight_value_changed(value):
	get_node("VBoxContainer/HBoxContainer8/Value").set_text("%d" % value)
	emit_signal("changed", self.to_params())
