extends Control

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

signal start_pressed

# Called when the node enters the scene tree for the first time.
func _ready():
	#get_node(".").set_exclusive(true)
	#get_node(".").popup_centered()
	var Game = load("Game.gd")
	get_node("VBoxContainer/TabContainer/Select Difficulty/VBoxContainer/ParameterTweaker").from_params(Game.medium_params())


func _on_difficulty_preset_pressed(button, value):
	var Game = load("Game.gd")
	var buttons = {
		get_node("VBoxContainer/TabContainer/Select Difficulty/VBoxContainer/HBoxContainer/Easy"): funcref(Game, "easy_params"),
		get_node("VBoxContainer/TabContainer/Select Difficulty/VBoxContainer/HBoxContainer/Medium"): funcref(Game, "medium_params"),
		get_node("VBoxContainer/TabContainer/Select Difficulty/VBoxContainer/HBoxContainer/Hard"): funcref(Game, "hard_params"),
		get_node("VBoxContainer/TabContainer/Select Difficulty/VBoxContainer/HBoxContainer/Custom"): null,
	}
	if value:
		for b in buttons.keys():
			if b != button:
				b.set_pressed(false)
		if buttons[button] != null:
			get_node("VBoxContainer/TabContainer/Select Difficulty/VBoxContainer/ParameterTweaker").from_params(buttons[button].call_func())

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_LevelCode_text_changed():
	var p = load("Game.gd").parse_levelcode(find_node("LevelCode").text)
	if p == null:
		find_node("Valid").set_text("Invalid level code")
		find_node("Valid").add_color_override("font_color", Color("ff0000"))
		find_node("Reset").set_disabled(true)
	else:
		find_node("Valid").set_text("Valid level code")
		find_node("Valid").add_color_override("font_color", Color("00ff00"))
		find_node("Reset").set_disabled(false)
		get_node("VBoxContainer/TabContainer/Import Level/ParameterTweaker").from_params(p)

func _on_Reset_pressed():
	var p = load("Game.gd").parse_levelcode(find_node("LevelCode").text)
	get_node("VBoxContainer/TabContainer/Import/ParameterTweaker").from_params(p)


func _on_Custom_toggled(button_pressed):
	_on_difficulty_preset_pressed(get_node("VBoxContainer/TabContainer/Select Difficulty/VBoxContainer/HBoxContainer/Custom"), button_pressed)


func _on_Hard_toggled(button_pressed):
	_on_difficulty_preset_pressed(get_node("VBoxContainer/TabContainer/Select Difficulty/VBoxContainer/HBoxContainer/Hard"), button_pressed)


func _on_Medium_toggled(button_pressed):
		_on_difficulty_preset_pressed(get_node("VBoxContainer/TabContainer/Select Difficulty/VBoxContainer/HBoxContainer/Medium"), button_pressed)


func _on_Easy_toggled(button_pressed):
	_on_difficulty_preset_pressed(get_node("VBoxContainer/TabContainer/Select Difficulty/VBoxContainer/HBoxContainer/Easy"), button_pressed)


func _on_ParameterTweaker_reset(params):
	find_node("GeneratedLevelCode").set_text(Marshalls.variant_to_base64(params))


func _on_ParameterTweaker_changed(params):
	find_node("GeneratedLevelCode").set_text(Marshalls.variant_to_base64(params))
	var custom = get_node("VBoxContainer/TabContainer/Select Difficulty/VBoxContainer/HBoxContainer/Custom")
	custom.set_pressed(true)



func _on_Start_pressed():
	var params = get_node("VBoxContainer/TabContainer/Select Difficulty/VBoxContainer/ParameterTweaker").to_params()
	if get_node("VBoxContainer/TabContainer/Import Level").is_visible():
		params = get_node("VBoxContainer/TabContainer/Import Level/ParameterTweaker").to_params()
	emit_signal("start_pressed", params)
