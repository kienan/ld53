extends Node


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
const camera_drag_threshold = 0.2
var camera_drag_start = null

# Called when the node enters the scene tree for the first time.
func _ready():
	# For non-game PRNG, eg. sounds
	randomize()

	var c = get_tree().get_nodes_in_group("character")[0]
	c.connect("character_movement", self, "_on_character_movement")
	c.connect("storage_changed", self, "_on_character_storage")
	get_node("UiLayer/PanelContainer/VBoxContainer/Capacity").set_text("Cargo %s" % c.capacity_str())
	var g = get_node("Game")
	g.connect("dropoff_score_changed", self, "_on_dropoff_score_changed")
	g.connect("game_ended", self, "_on_game_ended")
	
	var d = get_node("UiLayer/GameEndDialog")
	d.find_node("Continue").connect("pressed", self, "_on_continue")
	d.find_node("Restart").connect("pressed", self, "_on_restart")
	d.find_node("New").connect("pressed", self, "_on_new")
	d.find_node("Quit").connect("pressed", self, "_on_quit")
	d.find_node("LevelCode").set_text(g.get_levelcode())

func _on_character_movement(value):
	get_node("UiLayer/PanelContainer/VBoxContainer/Moved").set_text("Moved: %d" % value)
	get_node("UiLayer/PanelContainer/VBoxContainer/Score").set_text("Total score: %d" % get_node("Game").get_score())

func _on_character_storage(value):
	var c = get_tree().get_nodes_in_group("character")[0]
	get_node("UiLayer/PanelContainer/VBoxContainer/Capacity").set_text("Cargo %s" % c.capacity_str())

func _on_dropoff_score_changed(value):
	get_node("UiLayer/PanelContainer/VBoxContainer/Supplied").set_text("Supplied score: %d" % value[0])
	get_node("UiLayer/PanelContainer/VBoxContainer/Delivered").set_text("Delivered: %d" % value[1])
	get_node("UiLayer/PanelContainer/VBoxContainer/Score").set_text("Total score: %d" % get_node("Game").get_score())

func _on_game_ended():
	get_tree().paused = true
	var g = get_node("Game")
	var d = get_node("UiLayer/GameEndDialog")
	var score_data = g.get_supplied()
	d.find_node("Score").set_bbcode("[center]Score: %d[/center]" % g.get_score())
	d.find_node("Supplied").set_bbcode("%d / %d supplied\n%d points" % score_data.slice(0, 2))
	d.find_node("Moved").set_bbcode("%d moves" % g.get_moves())
	d.find_node("Delivered").set_bbcode("%d delivered" % score_data[3])	
	var path = ""
	for n in g.get_node("Character").path_history:
		path += "%s -> " % n.node_name
	if path.length() > 4:
		path = path.substr(0, path.length()-4)
	d.find_node("Path").set_bbcode("[center]Path[/center]\n%s" % path)
	d.find_node("Continue").set_visible(score_data[0] < score_data[1])
	if !score_data[0] < score_data[1]:
		get_node("UiAudio").set_stream(load("res://resources/untitled.wav"))
		get_node("UiAudio").play()
	d.find_node("LevelCode").set_text(g.get_levelcode())
	d.set_exclusive(true)
	d.popup_centered()

func _on_continue():
	get_node("UiLayer/GameEndDialog").set_visible(false)
	get_tree().paused = false

func _on_restart():
	get_node("Game").restart(get_node("Game").params)
	get_node("Camera2D").set_zoom(Vector2(1,1))
	get_node("Camera2D").set_offset(Vector2(512, 300))
	self._on_continue()
	
	# Reset UI
	_on_character_movement(0)
	_on_character_storage(0)
	_on_dropoff_score_changed([0, 0, 0])
	# From _ready
	var c = get_tree().get_nodes_in_group("character")[0]
	c.connect("character_movement", self, "_on_character_movement")
	c.connect("storage_changed", self, "_on_character_storage")
	get_node("UiLayer/PanelContainer/VBoxContainer/Capacity").set_text("Cargo %s" % c.capacity_str())
	var g = get_node("Game")
	g.connect("dropoff_score_changed", self, "_on_dropoff_score_changed")
	g.connect("game_ended", self, "_on_game_ended")

func _on_new():
	# Show the level setting popup
	get_node("UiLayer/GameEndDialog").set_visible(false)
	get_node("UiLayer/NewLevelDialog").set_exclusive(true)
	get_node("UiLayer/NewLevelDialog").popup_centered()

func _on_quit():
	get_tree().quit()

func _unhandled_input(event):
	if event is InputEventKey and event.pressed and !event.echo:
		# Quit on Escape press.
		if event.scancode == KEY_ESCAPE:
			var data = get_node("Game").get_supplied()
			var ended = data[0] >= data[1]
			if get_node("UiLayer/GameEndDialog").visible && !ended:
				self._on_continue()
			else:
				self._on_game_ended()
	if event is InputEventMouseButton and event.pressed:
		var zoom = get_node("Camera2D").get_zoom()
		if event.button_index == BUTTON_WHEEL_UP:
			zoom -= Vector2(.1, .1)
			get_node("Camera2D").set_zoom(zoom)
		elif event.button_index == BUTTON_WHEEL_DOWN:
			zoom += Vector2(.1, .1)
			get_node("Camera2D").set_zoom(zoom)
		if event.button_index == BUTTON_RIGHT:
			self.camera_drag_start = get_viewport().get_mouse_position()
	if event is InputEventMouseButton and event.button_index == BUTTON_RIGHT:
		if !event.pressed and self.camera_drag_start != null:
			self.camera_drag_start = null

func _process(delta):
	if self.camera_drag_start != null:
		var pos = get_viewport().get_mouse_position()
		var pos_delta = self.camera_drag_start - pos
		get_node("Camera2D").set_offset(
			get_node("Camera2D").get_offset() + pos_delta
		)
		self.camera_drag_start = pos

func _on_NewLevelDialog_start_pressed(params):
	get_node("UiLayer/NewLevelDialog").set_visible(false)
	get_node("Camera2D").set_zoom(Vector2(1,1))
	get_node("Camera2D").set_offset(Vector2(512, 300))
	get_node("Game").restart(params)
	self._on_continue()
	
	# Reset UI
	_on_character_movement(0)
	_on_character_storage(0)
	_on_dropoff_score_changed([0, 0, 0])
	# From _ready
	var c = get_tree().get_nodes_in_group("character")[0]
	c.connect("character_movement", self, "_on_character_movement")
	c.connect("storage_changed", self, "_on_character_storage")
	get_node("UiLayer/PanelContainer/VBoxContainer/Capacity").set_text("Cargo %s" % c.capacity_str())
	var g = get_node("Game")
	g.connect("dropoff_score_changed", self, "_on_dropoff_score_changed")
	g.connect("game_ended", self, "_on_game_ended")


func _on_Hide_toggled(button_pressed):
	get_node("UiLayer/PanelContainer/VBoxContainer").set_visible(!button_pressed)
	var size = get_node("UiLayer/PanelContainer").get_size()
	if button_pressed:
		size.y = 50
		get_node("UiLayer/PanelContainer").set_size(size)
	else:
		size.y = 600
		get_node("UiLayer/PanelContainer").set_size(size)


func _on_ResetCamera_pressed():
	get_node("Camera2D").set_offset(Vector2(512, 300))
	get_node("Camera2D").set_zoom(Vector2(1, 1))
