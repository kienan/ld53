extends Node2D

signal character_movement
signal arrived_at
signal storage_changed

var location = null
var offset = Vector2(0, 16)
var destination = null
# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var target = null
var links = {}

var capacity = 10
var stored = 0
var path_history = []
var movement_cost = 0

func capacity_str():
	if is_inf(self.capacity):
		return "%d / ∞" % self.stored
	return "%d / %d" % [self.stored, self.capacity]

func set_location(n, teleport = false):
	assert(n != null)
	self.path_history.append(n)
	if teleport:
		self.position = n.get_position() + self.offset
	else:
		self.destination = n.get_position() + self.offset
		if self.location != null:
			self.movement_cost += self.links[n].weight
			emit_signal("character_movement", self.movement_cost)
			get_node("Audio").play()
	self.location = n
	self.clear_links()
	self.clear_target()
	self.target = null;
	# Highlight links connected to this one that are traversable by the player
	for link in get_tree().get_nodes_in_group("links"):
		if link.start == n or link.end == n:
			link.get_node("Line").set_default_color(Color("98f9ac"))
			var target = link.start
			if link.start == n:
				target = link.end
			self.links[target] = link
		else:
			link.get_node("Line").set_default_color(Color("98a8f9"))
	# print("Links: ", self.links)
	

func clear_links():
	for link in self.links:
		self.links[self.target].get_node("Line").set_default_color(Color("98f9ac"))
		self.links[self.target].get_node("Label").remove_color_override("default_color")
		self.links[self.target].get_node("Label").set_scale(Vector2(1, 1))
	self.links = {}
		
func clear_target():
	if self.target != null && self.target in self.links:
		self.links[self.target].get_node("Line").set_default_color(Color("98f9ac"))
		self.links[self.target].get_node("Label").remove_color_override("default_color")
		self.links[self.target].get_node("Label").set_scale(Vector2(1, 1))
	self.target = null

func set_target(node):
	self.clear_target()
	self.target = node
	self.links[self.target].get_node("Line").set_default_color(Color("0fff2c"))
	self.links[self.target].get_node("Label").add_color_override("default_color", Color("ff00e0"))
	self.links[self.target].get_node("Label").set_scale(Vector2(2, 2))
	var audio = get_tree().get_current_scene().find_node("UiAudio")
	if audio:
		audio.set_stream(load("res://resources/Tap.wav"))
		audio.play()
	
func can_move(to):
	return self.destination == null && self.links.has(to)

func pickup(amount):
	self.stored += amount
	emit_signal("storage_changed", self.stored)
	self._on_storage_changed(self.stored)

func dropoff(amount):
	self.stored -= amount
	emit_signal("storage_changed", self.stored)
	self._on_storage_changed(self.stored)

func _on_storage_changed(value):
	var ratio = float(self.stored) / float(self.capacity)
	get_node("Box3").set_visible(ratio >= 0.75)
	get_node("Box2").set_visible(ratio >= 0.50)
	get_node("Box").set_visible(ratio > 0)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if destination != null:
		var p = self.get_position().move_toward(self.destination, delta * 500)
		self.set_position(p)
		if p.distance_to(self.destination) < 5:
			self.set_position(self.destination)
			self.destination = null
			get_node("Audio").stop()
			emit_signal("arrived_at", self.location)
	if destination == null and location != null:
		var p = get_global_mouse_position()
		var distance = null
		var closest = null
		for node in location.peers:
			if !is_instance_valid(node):
				continue
			var d = node.get_position().distance_to(p)
			if distance == null or d < distance:
				distance = d
				closest = node
		if self.target != closest:
			self.set_target(closest)
