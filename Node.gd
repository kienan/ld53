extends Node2D

var stored = 0
var capacity = 0
var texture_resource = ""
var value = 0
var node_name = ''

var peers = []
const drag_threshold = 0.2
var drag = false
var drag_hold = 0

func name_from_index(index):
	var n = char(index % 26 + 65)
	if index > 26:
		n += "%d" % index / 26
	self.set_node_name(n)

func set_node_name(value):
	self.node_name = value
	get_node("Label").set_text(self.node_name)
	
func is_full():
	return self.capacity >= self.stored

# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	add_to_group("nodes")
	if self.texture_resource != "":
		get_node("Sprite").set_texture(load(self.texture_resource))
	self.update_help()

func update_help():
	get_node("Help").set_tooltip("")
	get_node("Help").set_text("")
	
func path_to(destination):
	pass
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _process(delta):
	if self.drag:
		self.drag_hold += delta

func update_position(pos):
	self.set_position(pos)
	for link in get_tree().get_nodes_in_group("links"):
		if link.end == self or link.start == self:
			link.redraw_line()
	var c = get_tree().get_nodes_in_group("character")[0]
	if is_instance_valid(c):
		if c.location == self:
			c.set_position(self.get_position() + c.offset)

func _on_Help_gui_input(event):
	if event is InputEventMouseMotion:
		if self.drag and self.drag_hold > self.drag_threshold:
			self.set_position(get_global_mouse_position())
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT and event.is_doubleclick():
			if false:
				print(self)
				print("\tPeers: ", self.peers)
				var links = []
				for link in get_tree().get_nodes_in_group("links"):
					if link.start == self or link.end == self:
						links.append(link)
				print("\tLinks: ")
				for link in links:
					print("\t\t", link, " ", link.start, " <---> ", link.end)
				
			var c = get_tree().get_nodes_in_group("character")[0]
			if c != null and c.can_move(self):
				c.set_location(self)
		if event.button_index == BUTTON_LEFT and !event.is_doubleclick():
			if event.pressed:
				self.drag = true
			else:
				if self.drag:
					self.drag = false
					if self.drag_hold > self.drag_threshold:
						self.update_position(get_global_mouse_position())
					self.drag_hold = 0
		
