extends Node2D

var start = null
var end = null
var weight = 1

# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	add_to_group("links")
	get_node("Label").set_text("%d" % self.weight)
	get_node("Label").set_tooltip("Moving across this line costs {} energy".format([self.weight], "{}"))

func set_weight(value):
	assert(value > 0)
	self.weight = value
	get_node("Label").set_text("%d" % self.weight)
	get_node("Label").set_tooltip("Moving across this line costs {} energy".format([self.weight], "{}"))
	if self.weight >= 2:
		get_node("Label").set_modulate(Color("d7e65b"))
	if self.weight >= 3:
		get_node("Label").set_modulate(Color("e22c2c"))

func set_ends(a, b):
	assert(a != null)
	assert(b != null)
	self.start = a
	self.end = b
	self.redraw_line()
	
func redraw_line():
	get_node("Line").clear_points()
	get_node("Label").set_position(Vector2(
		abs(end.get_position().x + start.get_position().x) / 2,
		abs(end.get_position().y + start.get_position().y) / 2
	))
	get_node("Line").add_point(self.start.get_position())
	get_node("Line").add_point(self.end.get_position())


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
