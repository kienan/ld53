extends "res://Node.gd"

func _init():
	self.texture_resource = "res://resources/empty.png"
# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	get_node("Help").set_tooltip("Empty spaces neither take nor give cargo")
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
