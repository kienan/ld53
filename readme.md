# LD53: Delivery

Pickup and deliver things around an area, trying to minimize how far you travel.

# Tools

 * Godot 3
 * GIMP
 * Audacity

# External Resources

 * Super Bubble font: https://www.dafont.com/super-bubble.font
 * Bitstream Vera Sans Nerd: https://github.com/ryanoasis/nerd-fonts
 * Satellite shots from google, remixed for background
