extends "res://Node.gd"

var empty_sounds = [
	preload("res://resources/NoMore.wav"),
	preload("res://resources/ThatsAll.wav"),
]

var load_sounds = [
	preload("res://resources/Thunk.wav"),
]

func _init():
	self.texture_resource = "res://resources/pickup.png"
	self.stored = INF


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	self.add_to_group("pickup")

func stored_str():
	if is_inf(self.stored):
		return "\u221E"
	return "{}".format([self.stored], "{}")

func add_stored(value):
	self.stored += value
	self.update_help()

func set_stored(value):
	self.stored = value
	self.update_help()

func drain(amount):
	if amount <= 0:
		return
	self.stored -= amount
	get_node("Audio").set_stream(self.load_sounds[randi() % self.load_sounds.size()])
	get_node("Audio").play()
	self.update_help()

func update_help():
	get_node("Help").set_tooltip("Go here to replenish your stock\nThere are {} items available here".format([stored_str()], "{}"))
	get_node("Help").set_text("{}".format([stored_str()], "{}"))
	if self.stored <= 0:
		get_node("Sprite").set_modulate(Color("da0000"))
		get_node("Help").set_self_modulate(Color("da0000"))
		
		var a = get_node("Audio")
		a.set_stream(self.empty_sounds[randi() % self.empty_sounds.size()])
		a.play()
		
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
