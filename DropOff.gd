extends "res://Node.gd"

var full_sounds = [
	preload("res://resources/Coins1.wav"),
	preload("res://resources/Coins2.wav"),
	preload("res://resources/Thanks.wav"),
	preload("res://resources/YoureTheBest.wav"),
]
var dropoff_sounds = [
	preload("res://resources/Items.wav"),
	preload("res://resources/Thunk.wav"),
	preload("res://resources/Scribble.wav"),
	preload("res://resources/Paper2.wav"),
]

func _init():
	self.capacity = 1
	self.value = 1
	self.texture_resource = "res://resources/dropoff.png"

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	self.add_to_group("dropoff")

func receive(amount):
	if amount <= 0:
		return
	self.stored += amount
	self.update_help(true)

func update_help(audio = false):
	get_node("Help").set_text("{} / {}\n{}".format([self.stored, self.capacity, self.value], "{}"))
	if self.stored < self.capacity:
		get_node("Help").set_tooltip("Drop off {} more to satistify delivery requirements.\nWhen satisfied {} will be added to your score".format([self.capacity - self.stored, self.value], "{}"))
	else:
		get_node("Help").set_tooltip("Fully supplied")
	if self.stored >= self.capacity:
		get_node("Sprite").set_modulate(Color("00ea81"))
		get_node("Help").set_self_modulate(Color("00ea81"))
		if audio:
			get_node("Audio").set_stream(
				self.full_sounds[randi() % self.full_sounds.size()]
			)
			get_node("Audio").play()
	else:
		if audio:
			get_node("Audio").set_stream(
				self.dropoff_sounds[randi() % self.dropoff_sounds.size()]
			)
			get_node("Audio").play()
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
