extends "res://Node.gd"

func _init():
	self.texture_resource = "res://resources/start.png"
# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	self.add_to_group("start")
	get_node("Help").set_tooltip("@TODO: Use an action here to end game early")

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
